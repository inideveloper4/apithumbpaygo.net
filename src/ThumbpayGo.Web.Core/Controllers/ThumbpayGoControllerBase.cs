using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace ThumbpayGo.Controllers
{
    public abstract class ThumbpayGoControllerBase: AbpController
    {
        protected ThumbpayGoControllerBase()
        {
            LocalizationSourceName = ThumbpayGoConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
