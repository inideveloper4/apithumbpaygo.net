﻿using ThumbpayGo.Configuration.Ui;

namespace ThumbpayGo.Web.Views.Shared.Components.RightSideBar
{
    public class RightSideBarViewModel
    {
        public UiThemeInfo CurrentTheme { get; set; }
    }
}
