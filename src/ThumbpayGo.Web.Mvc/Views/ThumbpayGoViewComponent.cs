﻿using Abp.AspNetCore.Mvc.ViewComponents;

namespace ThumbpayGo.Web.Views
{
    public abstract class ThumbpayGoViewComponent : AbpViewComponent
    {
        protected ThumbpayGoViewComponent()
        {
            LocalizationSourceName = ThumbpayGoConsts.LocalizationSourceName;
        }
    }
}
