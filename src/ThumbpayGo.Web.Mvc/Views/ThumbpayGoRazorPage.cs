﻿using Microsoft.AspNetCore.Mvc.Razor.Internal;
using Abp.AspNetCore.Mvc.Views;
using Abp.Runtime.Session;

namespace ThumbpayGo.Web.Views
{
    public abstract class ThumbpayGoRazorPage<TModel> : AbpRazorPage<TModel>
    {
        [RazorInject]
        public IAbpSession AbpSession { get; set; }

        protected ThumbpayGoRazorPage()
        {
            LocalizationSourceName = ThumbpayGoConsts.LocalizationSourceName;
        }
    }
}
