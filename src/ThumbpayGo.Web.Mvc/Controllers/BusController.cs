﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Threading.Tasks;
using ThumbpayGo.Bus;
using ThumbpayGo.Controllers;
using System.Linq;
using Abp.Web.Models;
using System;
using ThumbpayGo.ETravelSmart;

namespace ThumbpayGo.Web.Mvc.Controllers
{
    public class BusController :ThumbpayGoControllerBase
    {
        private IBusAppService _busAppService;
        public BusController(IBusAppService busAppService) {
            _busAppService = busAppService;
        }

        public IActionResult Index() {
            return View();
        }
        public IActionResult BusList() {
            return View();
        }
        public IActionResult BusConfirm() {
            return View();
        }
        public IActionResult Payment() {
            return View();
        }
        [DontWrapResult]
        public async Task<JsonResult> GetStationList() {
            var stations = await _busAppService.GetStations();
            return Json(stations.stationList.Select(r => r.stationName));
        }
        [DontWrapResult]
        public async Task<JsonResult> GetAvailableBuses(string sourceCity, string destinationCity, DateTime doj) {
            var buses = await _busAppService.GetAvailableBuses(sourceCity, destinationCity, doj.ToString("yyyy-MM-dd"));
            return Json(buses.apiAvailableBuses);
        }
        [DontWrapResult]
        public async Task<PartialViewResult> GetBusLayout(string sourceCity, string destinationCity, DateTime doj, int inventoryType, int routeScheduleId) {
            var busLayOut = await _busAppService.GetBusLayout(sourceCity, destinationCity, doj.ToString("yyyy-MM-dd"), inventoryType, routeScheduleId);
            return PartialView("_SeatLayout", busLayOut);
        }
        [DontWrapResult]
        [HttpPost]
        public async Task<JsonResult> BlockTicket([FromBody] BlockTicketRequest blockTicketRequest) {
            try {

                var busLayOut = await _busAppService.BlockTicket(blockTicketRequest);
                return Json(busLayOut);
            } catch(Exception ex) {
                return Json(ex.Message);
            }
        }
        [DontWrapResult]
        public async Task<JsonResult> GetRTCUpdatedFare(string blockTicketKey) {
            try {
                return Json(await _busAppService.GetRTCUpdatedFare(blockTicketKey));
            } catch(Exception ex) {
                return Json(ex.Message);
            }
        }
        [DontWrapResult]
        public async Task<JsonResult> SeatBooking(string blockTicketKey) {
            try {
                return Json(await _busAppService.SeatBooking(blockTicketKey));
            } catch(Exception ex) {
                return Json(ex.Message);
            }
        }
        [DontWrapResult]
        public async Task<JsonResult> GetBookedTicket(string ETSTNumber) {
            try {

                return Json(await _busAppService.GetBookedTicket(ETSTNumber));
            } catch(Exception ex) {
                return Json(ex.Message);
            }
        }
        [DontWrapResult]
        public async Task<JsonResult> CancelTicketConfirmation(CancelConfirmationTicketRequest cancelTicketRequest) {
            try {
                return Json(await _busAppService.CancelTicketConfirmation(cancelTicketRequest));
            } catch(Exception ex) {
                return Json(ex.Message);
            }
        }
        [DontWrapResult]
        public async Task<JsonResult> CancelTicket(CancelTicketRequest cancelTicketRequest) {
            try {
                return Json(await _busAppService.CancelTicket(cancelTicketRequest));
            } catch(Exception ex) {
                return Json(ex.Message);
            }
        }
        [DontWrapResult]
        public async Task<JsonResult> GetMyPlanAndCurrentBalance() {
            try {
                return Json(await _busAppService.GetMyPlanAndCurrentBalance());
            } catch(Exception ex) {
                return Json(ex.Message);
            }
        }
        public IActionResult Profile() {
            return View();
        }
        public IActionResult BusList1() {

            return View();
        }
        public IActionResult RedirectToConfirmBooking(string ETSTNumber,[FromBody] BlockTicketRequest blockTicketRequest) {
            try {
                TempData["blockTicketRequest"] = blockTicketRequest; 
                return RedirectToAction("CofirmBoking");
            } catch(Exception ex) {
                return Json(ex.Message);
            }
        }
        public IActionResult CofirmBoking() {
            try 
                {
                BlockTicketRequest blockTicketRequest = (BlockTicketRequest)TempData["blockTicketRequest"];
                return View();
            } catch(Exception ex) {
                return Json(ex.Message);
            }
        }
    }
}