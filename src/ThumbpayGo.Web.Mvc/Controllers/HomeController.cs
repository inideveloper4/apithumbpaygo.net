﻿using Microsoft.AspNetCore.Mvc;
using Abp.AspNetCore.Mvc.Authorization;
using ThumbpayGo.Controllers;

namespace ThumbpayGo.Web.Controllers
{
    [AbpMvcAuthorize]
    public class HomeController : ThumbpayGoControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
	}
}
