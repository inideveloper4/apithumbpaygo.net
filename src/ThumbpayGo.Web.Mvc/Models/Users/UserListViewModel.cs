using System.Collections.Generic;
using ThumbpayGo.Roles.Dto;
using ThumbpayGo.Users.Dto;

namespace ThumbpayGo.Web.Models.Users
{
    public class UserListViewModel
    {
        public IReadOnlyList<UserDto> Users { get; set; }

        public IReadOnlyList<RoleDto> Roles { get; set; }
    }
}
