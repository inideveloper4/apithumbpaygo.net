﻿using System.Collections.Generic;
using ThumbpayGo.Roles.Dto;

namespace ThumbpayGo.Web.Models.Roles
{
    public class RoleListViewModel
    {
        public IReadOnlyList<RoleListDto> Roles { get; set; }

        public IReadOnlyList<PermissionDto> Permissions { get; set; }
    }
}
