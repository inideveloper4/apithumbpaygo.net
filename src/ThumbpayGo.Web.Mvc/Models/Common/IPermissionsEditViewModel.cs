﻿using System.Collections.Generic;
using ThumbpayGo.Roles.Dto;

namespace ThumbpayGo.Web.Models.Common
{
    public interface IPermissionsEditViewModel
    {
        List<FlatPermissionDto> Permissions { get; set; }
    }
}