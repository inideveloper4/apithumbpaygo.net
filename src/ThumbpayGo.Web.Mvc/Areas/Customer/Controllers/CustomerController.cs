﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ThumbpayGo.Controllers;

namespace ThumbpayGo.Web.Mvc.Areas.Customer.Controllers
{
    [Area("customer")]
    public class CustomerController : ThumbpayGoControllerBase
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}