﻿using Abp.Configuration.Startup;
using Abp.Localization.Dictionaries;
using Abp.Localization.Dictionaries.Xml;
using Abp.Reflection.Extensions;

namespace ThumbpayGo.Localization
{
    public static class ThumbpayGoLocalizationConfigurer
    {
        public static void Configure(ILocalizationConfiguration localizationConfiguration)
        {
            localizationConfiguration.Sources.Add(
                new DictionaryBasedLocalizationSource(ThumbpayGoConsts.LocalizationSourceName,
                    new XmlEmbeddedFileLocalizationDictionaryProvider(
                        typeof(ThumbpayGoLocalizationConfigurer).GetAssembly(),
                        "ThumbpayGo.Localization.SourceFiles"
                    )
                )
            );
        }
    }
}
