﻿using Abp.MultiTenancy;
using ThumbpayGo.Authorization.Users;

namespace ThumbpayGo.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
