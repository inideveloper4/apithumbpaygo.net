﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Abp.Timing;
using ThumbpayGo.Authorization.Users;

namespace ThumbpayGo.Authorization
{
    public class WhitelevelUser: Entity, IHasCreationTime
    {
        public WhitelevelUser()
        {
            CreationTime = Clock.Now;
        }
        [ForeignKey("User")]
        public long UserId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public string IpAddress { get; set; }
        public string PermanentAddress { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreationTime { get; set; }
        public virtual Wallet Wallet { get; set; }
        public virtual User User { get; set; }
    }
}

