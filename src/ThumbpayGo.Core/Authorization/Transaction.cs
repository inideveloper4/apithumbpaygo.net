﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Abp.Timing;

namespace ThumbpayGo.Authorization
{
    public class Transaction: Entity, IHasCreationTime
    {
        public Transaction()
        {
            CreationTime = Clock.Now;
        }
        [ForeignKey("Wallet")]
        public int WalletId { get; set; }
        public decimal DebitAmount { get; set; }
        public decimal CreditAmount { get; set; }
        public DateTime CreationTime { get; set; } 
        public virtual Wallet Wallet { get; set; }
    }
}
