﻿using Abp.Authorization;
using ThumbpayGo.Authorization.Roles;
using ThumbpayGo.Authorization.Users;

namespace ThumbpayGo.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
