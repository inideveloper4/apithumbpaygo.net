﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Abp.Timing;

namespace ThumbpayGo.Authorization
{
    public  class Wallet: Entity, IHasCreationTime, IHasModificationTime
    {
        public Wallet()
        {
            CreationTime = Clock.Now;
        }
        [ForeignKey("WhitelevelUser")]
        public int WhiteLevelId { get; set; }
        public decimal TotalAmount { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime? LastModificationTime { get; set; }
        public virtual WhitelevelUser WhitelevelUser  { get; set; }
        public virtual List<Transaction> Transactions { get; set; }
    }
}
