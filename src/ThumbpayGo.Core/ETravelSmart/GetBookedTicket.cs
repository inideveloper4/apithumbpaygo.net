﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Abp.Timing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThumbpayGo.ETravelSmart
{
    public class GetBookedTicket : Entity, IHasCreationTime
    {

        public GetBookedTicket()
        {
            CreationTime = Clock.Now;
        }
        public ApiStatus apiStatus { get; set; }
        public TravelerDetails travelerDetails { get; set; }
        public string ticketStatus { get; set; } // CONFIRMED, CANCELLED, TRAVELLED,SERVICE_CANCELLED
        public string inventoryType { get; set; }
        public string sourceCity { get; set; }
        public string destinationCity { get; set; }
        public string journeyDate { get; set; }
        public string departureTime { get; set; }
        public string routeScheduleId { get; set; }
        public string serviceProvider { get; set; }
        public string service_type { get; set; }  //        (Volvo, semi sleeper)
        public string serviceId { get; set; }
        public string serviceProviderContact { get; set; }
        public string boardingPoint { get; set; }
        public string droppingPoint { get; set; }
        public string ETSTNumber { get; set; }
        public string opPNR { get; set; }
        public double commPCT { get; set; }
        public string cancellationPolicy { get; set; }
        public DateTime bookingDate { get; set; }
        public DateTime Date { get; set; }
        public double refundAmount { get; set; }
        public string tripCode { get; set; }
        public DateTime CreationTime { get; set; }
    }


    public class TravelerDetails
    {
        [Key,ForeignKey("GetBookedTicket")]
        public int GetBookedTicketId { get; set; }
        public string name { get; set; }
        public string lastName { get; set; }
        public string seatNo { get; set; }
        public long age { get; set; }
        public string gender { get; set; }
        public double fare { get; set; }
        public GetBookedTicket GetBookedTicket { get; set; }
    }
}





