﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Abp.Timing;
using System;

namespace ThumbpayGo.ETravelSmart
{
    public class SeatBooking: Entity, IHasCreationTime
    {
        public SeatBooking()
        {
            CreationTime = Clock.Now;
        }
        public string opPNR { get; set; }
        public string etstnumber { get; set; }
        public string commPCT { get; set; }
        public decimal totalFare { get; set; }
        public string cancellationPolicy { get; set; }
        public int tripCode { get; set; }
        public ApiStatus apiStatus { get; set; }
        public DateTime CreationTime { get; set; }
        public int inventoryType { get; set; }
    }

}
