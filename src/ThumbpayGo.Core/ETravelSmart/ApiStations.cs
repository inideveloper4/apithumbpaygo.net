﻿using Abp.Domain.Entities;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace ThumbpayGo.ETravelSmart
{
    [ResponseCache(VaryByHeader = "User-Agent", Duration = 30)]
    public  class ApiStations
    {
        public ApiStatus apiStatus { get; set; }
        public List<ApiStation> stationList { get; set; }
    }
    public class ApiStation
    {
        public long stationId { get; set; }
        public string stationName { get; set; }
    }
    public class ApiStatus: Entity
    {

        public string message { get; set; }
        public Boolean success { get; set; }
    }
}
