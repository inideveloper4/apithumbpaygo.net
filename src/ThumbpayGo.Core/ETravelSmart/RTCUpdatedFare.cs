﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThumbpayGo.ETravelSmart
{
    public class RTCUpdatedFare
    {
        public int convenienceFee { get; set; }
        public int bookingFee { get; set; }
        public int reservationFee { get; set; }
        public int tollFee { get; set; }
        public int otherCharges { get; set; }
        public int previousFare { get; set; }
        public int updatedFare { get; set; }
        public ApiStatus apiStatus { get; set; }
    }
}
