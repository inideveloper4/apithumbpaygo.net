﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThumbpayGo.ETravelSmart
{
     public class BaseModel
    {
        public BaseModel()
        {
            DateCreated = DateTime.Now;
        }
        [Required]
        public string UserName { get; set; }
        public DateTime DateCreated { get; set; }
    }
}
