﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Abp.Timing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ThumbpayGo.ETravelSmart
{
    public class BlockTicketRequest : Entity, IHasCreationTime
    {
        public BlockTicketRequest()
        {
            CreationTime = Clock.Now;
        }
        public string sourceCity { get; set; }
        public string destinationCity { get; set; }
        public DateTime doj { get; set; }
        public long routeScheduleId { get; set; }
        public string customerName { get; set; }
        public string customerLastName { get; set; }
        public string customerEmail { get; set; }
        public string customerPhone { get; set; }
        public string emergencyPhNumber { get; set; }
        public string customerAddress { get; set; }
        public int inventoryType { get; set; }
        public DateTime CreationTime { get; set; }
        public virtual List<BlockSeatPaxDetail> blockSeatPaxDetails { get; set; }
        [NotMapped]
        public virtual BoardDropPointData boardingPoint { get; set; }
         [NotMapped]
        public virtual BoardDropPointData droppingPoint { get; set; }
    }
    public class BlockSeatPaxDetail: Entity
    {
        [ForeignKey("BlockTicketRequest")]
        public int BlockTicketRequestId { get; set; }
        public int age { get; set; }
        public string name { get; set; }
        public string seatNbr { get; set; }
        public string sex { get; set; }
        public decimal fare { get; set; }
        public decimal serviceTaxAmount { get; set; }
        public decimal operatorServiceChargeAbsolute { get; set; }
        public decimal totalFareWithTaxes { get; set; }
        public bool ladiesSeat { get; set; }
        public string lastName { get; set; }
        public string mobile { get; set; }
        public string title { get; set; }
        public string email { get; set; }
        public string idType { get; set; }
        public string idNumber { get; set; }
        public string nameOnId { get; set; }
        public bool primary { get; set; }
        public bool ac { get; set; }
        public bool sleeper { get; set; }
        public virtual BlockTicketRequest BlockTicketRequest { get; set; }
    }
    public class BlockTicketResponse: Entity
    {
        [ForeignKey("BlockTicketRequest")]
        public int BlockTicketRequestId { get; set; }
        public string blockTicketKey { get; set; }
        public string inventoryType { get; set; }
        public ApiStatusData apiStatus { get; set; }
        public virtual BlockTicketRequest BlockTicketRequest { get; set; }
    }
    public class BoardDropPointData
    {
        [ForeignKey("BlockTicketRequest")]
        public int BlockTicketRequestId { get; set; }
        [Key]
        public int PKID { get; set; }
        public string location { get; set; }
        public long id { get; set; }
        public string time { get; set; }
        public virtual BlockTicketRequest BlockTicketRequest { get; set; }
    }

     public class ApiStatusData: Entity
    {
        [ForeignKey("BlockTicketRequest")]
        public int BlockTicketRequestId { get; set; }
        public string message { get; set; }
        public Boolean success { get; set; }
        public virtual BlockTicketRequest BlockTicketRequest { get; set; }

    }
}
