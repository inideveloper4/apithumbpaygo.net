﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThumbpayGo.ETravelSmart
{
    public class ApiAvailableBuses
    {
        public List<AvailableBus> apiAvailableBuses { get; set; }
        public ApiStatus apiStatus { get; set; }
    }

    public class AvailableBus
    {
        public int inventoryType { get; set; }
        public string operatorName { get; set; }
        public string departureTime { get; set; }
        public Boolean mTicketAllowed { get; set; }
        public Boolean idProofRequired { get; set; }
        public long serviceId { get; set; }
        public decimal fare { get; set; }
        public string busType { get; set; }
        public long routeScheduleId { get; set; }
        public int availableSeats { get; set; }
        public Boolean partialCancellationAllowed { get; set; }
        public string arrivalTime { get; set; }
        public long operatorId { get; set; }
        public decimal commPCT { get; set; }
        public Boolean isGetLayoutByBPDP { get; set; }
        public Boolean isFareUpdateRequired { get; set; }
        public Boolean isOpTicketTemplateRequired { get; set; }
        public Boolean isOpLogoRequired { get; set; }
        public Boolean is_child_concession { get; set; }
        public Boolean isRTC { get; set; }
        public string cancellationPolicy { get; set; }
        public List<BoardDropPoint> boardingPoints { get; set; }
        public List<BoardDropPoint> droppingPoints { get; set; }

    }
    public class CancellationPolicy
    {

        public string cutoffTime { get; set; }
        public string refundInPercentage { get; set; }
    }
    public class BoardDropPoint
    {


        public string location { get; set; }
        public long id { get; set; }
        public string time { get; set; }
    }
}
