﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThumbpayGo.ETravelSmart
{
    public class BusLayout
    {
        public List<seat> seats { get; set; }
        public string boardingPoints { get; set; }
        public string droppingPoints { get; set; }
        public int inventoryType { get; set; }
        public Boolean serviceTaxApplicable { get; set; }
        public ApiStatus apiStatus { get; set; }
    }
    public class seat
    {
        public int length { get; set; }
        public string id { get; set; }
        public int width { get; set; }
        public Boolean available { get; set; }
        public decimal fare { get; set; }
        public decimal totalFareWithTaxes { get; set; }
        public decimal serviceTaxAmount { get; set; }
        public decimal serviceTaxPer { get; set; }
        public decimal operatorServiceChargeAbsolute { get; set; }
        public decimal operatorServiceChargePercent { get; set; }
        public Boolean ac { get; set; }
        public Boolean sleeper { get; set; }
        public int row { get; set; }
        public int column { get; set; }
        public int zIndex { get; set; }
        public string commission { get; set; }
        public Boolean ladiesSeat { get; set; }
        public string bookedBy { get; set; }
    }
}
