﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThumbpayGo.ETravelSmart
{
    public class MyPlanAndCurrentBalance
    {
        public string userType { get; set; }
        public string planName { get; set; }
        public string rmName { get; set; }
        public string planNature { get; set; }
        public string product { get; set; }
        public string allowedIPs { get; set; }
        public string referredBy { get; set; }
        public string rmEmail { get; set; }
        public string domainName { get; set; }
        public string notificationEnabled { get; set; }
        public string inventoryTypes { get; set; }
        public string rmContactNumber { get; set; }
        public string registrationDate { get; set; }
        public string renewalDate { get; set; }
        public string planDescription { get; set; }
        public string fixedCommission { get; set; }
        public string dynamicComission { get; set; }
        public string serviceCharges { get; set; }
        public string registrationAmount { get; set; }
        public string apiUserLogin { get; set; }
        public string balanceAmount { get; set; }
        public string paidRegistrationAmount { get; set; }
        public string lowBalanceAmount { get; set; }
        public ApiStatus apiStatus { get; set; }
    }
}
