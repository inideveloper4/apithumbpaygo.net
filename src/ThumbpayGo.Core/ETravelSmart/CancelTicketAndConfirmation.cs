﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Abp.Timing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThumbpayGo.ETravelSmart
{
    public class CancelTicketRequest: Entity, IHasCreationTime
    {
        public CancelTicketRequest()
        {
            CreationTime = Clock.Now;
        }
        public string etsTicketNo { get; set; }
        public string seatNbrsToCancelStr
        {
            get
            {
                return seatNbrsToCancelStr;
            }
            set
            {
                seatNbrsToCancel = value.Length > 0 ? value.Split(',').ToList() : new List<string>();
            }
        }
        [NotMapped]
        public List<string> seatNbrsToCancel
        {
            get
            {
                return seatNbrsToCancel;
            }
            set
            {
               seatNbrsToCancelStr = value.Count > 0 ? string.Join(",", value) : "";
            }
        }
        public DateTime CreationTime { get; set; }
        public CancelTicketResponse CancelTicketResponse { get; set; }
    }
    public class CancelTicketResponse: Entity
    {
        [ForeignKey("CancelTicketRequest")]
        public int CancelTicketRequestId { get; set; }
        public double totalTicketFare { get; set; }
        public string totalRefundAmount { get; set; }
        public string cancelChargesPercentage { get; set; }
        public double cancellationCharges { get; set; }
        public ApiStatus apiStatus { get; set; }
        public bool cancellable { get; set; }
        public bool partiallyCancellable { get; set; }
        public CancelTicketRequest CancelTicketRequest { get; set; }
    }
    public class CancelConfirmationTicketRequest : Entity, IHasCreationTime
    {
        public CancelConfirmationTicketRequest()
        {
            CreationTime = Clock.Now;
        }
        public string etsTicketNo { get; set; }
        public string seatNbrsToCancelStr
        {
            get
            {
                return seatNbrsToCancelStr;
            }
            set
            {
                seatNbrsToCancel = value.Length > 0 ? value.Split(',').ToList() : new List<string>();
            }
        }
        [NotMapped]
        public List<string> seatNbrsToCancel
        {
            get
            {
                return seatNbrsToCancel;
            }
            set
            {
                seatNbrsToCancelStr = value.Count > 0 ? string.Join(",", value) : "";
            }
        }
        public DateTime CreationTime { get; set; }
        public CancelConfirmationTicketResponse CancelConfirmationTicketResponse { get; set; }
    }
    public class CancelConfirmationTicketResponse : Entity
    {
        [ForeignKey("CancelConfirmationTicketRequest")]
        public int CancelConfirmationTicketRequestId { get; set; }
        public double totalTicketFare { get; set; }
        public string totalRefundAmount { get; set; }
        public string cancelChargesPercentage { get; set; }
        public double cancellationCharges { get; set; }
        public ApiStatus apiStatus { get; set; }
        public bool cancellable { get; set; }
        public bool partiallyCancellable { get; set; }
        public CancelConfirmationTicketRequest CancelConfirmationTicketRequest { get; set; }
    }
}
