﻿namespace ThumbpayGo
{
    public class ThumbpayGoConsts
    {
        public const string LocalizationSourceName = "ThumbpayGo";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;
    }
}
