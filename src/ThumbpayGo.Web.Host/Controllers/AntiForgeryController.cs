using Microsoft.AspNetCore.Antiforgery;
using ThumbpayGo.Controllers;

namespace ThumbpayGo.Web.Host.Controllers
{
    public class AntiForgeryController : ThumbpayGoControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
