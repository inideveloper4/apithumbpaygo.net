﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using ThumbpayGo.Configuration;

namespace ThumbpayGo.Web.Host.Startup
{
    [DependsOn(
       typeof(ThumbpayGoWebCoreModule))]
    public class ThumbpayGoWebHostModule: AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public ThumbpayGoWebHostModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(ThumbpayGoWebHostModule).GetAssembly());
        }
    }
}
