using System.ComponentModel.DataAnnotations;

namespace ThumbpayGo.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}