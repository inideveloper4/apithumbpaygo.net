﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using ThumbpayGo.Configuration.Dto;

namespace ThumbpayGo.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : ThumbpayGoAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}
