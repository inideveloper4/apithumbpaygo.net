﻿using System.Threading.Tasks;
using ThumbpayGo.Configuration.Dto;

namespace ThumbpayGo.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}
