﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ThumbpayGo.MultiTenancy.Dto;

namespace ThumbpayGo.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}

