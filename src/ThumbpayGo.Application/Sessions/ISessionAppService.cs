﻿using System.Threading.Tasks;
using Abp.Application.Services;
using ThumbpayGo.Sessions.Dto;

namespace ThumbpayGo.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}
