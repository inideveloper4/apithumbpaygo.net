﻿using Abp.Domain.Repositories;
using Abp.Web.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using ThumbpayGo.ETravelSmart;
namespace ThumbpayGo.Bus
{
    public class BusAppService :ThumbpayGoAppServiceBase, IBusAppService
    {

        IRepository<BlockTicketRequest> _blockTicketRepository;
        IRepository<CancelTicketRequest> _cancelTicketRepository;
        IRepository<CancelConfirmationTicketRequest> _cancelConfirmationTicketRepository;
        IRepository<GetBookedTicket> _getBookedTicketRepository;
        IRepository<SeatBooking> _seatBookingRepository;
        IRepository<BlockTicketResponse> _blockTicketResponseRepository;
        public BusAppService(IRepository<BlockTicketRequest> blockTicketRepository,
            IRepository<CancelTicketRequest> cancelTicketRepository,
             IRepository<CancelConfirmationTicketRequest> cancelConfirmationTicketRepository,
             IRepository<GetBookedTicket> getBookedTicketRepository,
             IRepository<SeatBooking> seatBookingRepository,
              IRepository<BlockTicketResponse> blockTicketResponseRepository

            ) {
            _blockTicketRepository = blockTicketRepository;
            _cancelTicketRepository = cancelTicketRepository;
            _cancelConfirmationTicketRepository = cancelConfirmationTicketRepository;
            _getBookedTicketRepository = getBookedTicketRepository;
            _seatBookingRepository = seatBookingRepository;
            _blockTicketResponseRepository = blockTicketResponseRepository;
            BusSevaBaseUrl = "http://test.etravelsmart.com/etsAPI/api/";
            username = "ThumbpayApi";
            password = "thumbpay@123";
        }
        public string BusSevaBaseUrl { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string MerchantId { get; set; }
        //[NotMapped]
        public void DigestiveAuth(HttpWebRequest request, Uri url) {
            NetworkCredential myNetworkCredential = new NetworkCredential(username, password);
            CredentialCache myCredentialCache = new CredentialCache();
            myCredentialCache.Add(url, "Digest", myNetworkCredential);
            request.PreAuthenticate = true;
            request.Credentials = myCredentialCache;
        }

        public async Task<ApiStations> GetStations() {
            try {
                ApiStations apiStations;
                Uri url = new Uri(BusSevaBaseUrl + "getStations");
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.ContentType = "application/json";
                request.Method = "GET";
                DigestiveAuth(request, url);
                var response = (HttpWebResponse)await request.GetResponseAsync();
                using(var streamReader = new StreamReader(response.GetResponseStream())) {
                    var result = await streamReader.ReadToEndAsync();
                    apiStations = JsonConvert.DeserializeObject<ApiStations>(result);
                }
                return apiStations;
            } catch(Exception ex) {
                throw new Exception(ex.Message);
            }
        }
        public async Task<ApiAvailableBuses> GetAvailableBuses(string sourceCity, string destinationCity, string doj) {
            try {
                ApiAvailableBuses apiAvailableBuses;
                Uri url = new Uri(BusSevaBaseUrl + "getAvailableBuses?sourceCity=" + sourceCity + "&destinationCity=" + destinationCity + "&doj=" + doj);
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.ContentType = "application/json";
                request.Method = "GET";
                DigestiveAuth(request, url);
                var response = (HttpWebResponse)await request.GetResponseAsync();
                using(var streamReader = new StreamReader(response.GetResponseStream())) {
                    var result = await streamReader.ReadToEndAsync();
                    apiAvailableBuses = JsonConvert.DeserializeObject<ApiAvailableBuses>(result);
                }
                return apiAvailableBuses;
            } catch(Exception ex) {
                throw new Exception(ex.Message);
            }

        }

        public async Task<BusLayout> GetBusLayout(string sourceCity, string destinationCity, string doj, int inventoryType, int routeScheduleId) {
            try {
                BusLayout busLayout;
                Uri url = new Uri(BusSevaBaseUrl + "getBusLayout?sourceCity=" + sourceCity + "&destinationCity=" + destinationCity + "&doj=" + doj + "&inventoryType=" + inventoryType + "&routeScheduleId=" + routeScheduleId);
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.ContentType = "application/json";
                request.Method = "GET";
                DigestiveAuth(request, url);
                var response = (HttpWebResponse)await request.GetResponseAsync();
                using(var streamReader = new StreamReader(response.GetResponseStream())) {
                    var result = await streamReader.ReadToEndAsync();
                    busLayout = JsonConvert.DeserializeObject<BusLayout>(result);
                }
                return busLayout;
            } catch(Exception ex) {
                throw new Exception(ex.Message);
            }
        }
        public async Task<BlockTicketResponse> BlockTicket(BlockTicketRequest blockTicketRequest) {
            try {
                 for(var i=0; i< blockTicketRequest.blockSeatPaxDetails.Count; i++) {
                    blockTicketRequest.blockSeatPaxDetails[i].BlockTicketRequest = new BlockTicketRequest();
                }
                await _blockTicketRepository.InsertOrUpdateAsync(blockTicketRequest);
               
                BlockTicketResponse blockTicketResponse;
                Uri url = new Uri(BusSevaBaseUrl + "blockTicket");
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.ContentType = "application/json";
                request.Method = "POST";
                DigestiveAuth(request, url);
                using(var streamWriter = new StreamWriter(request.GetRequestStream())) {
                    string json = JsonConvert.SerializeObject(blockTicketRequest);
                    streamWriter.Write(json);
                }
                var response = (HttpWebResponse)await request.GetResponseAsync();
                using(var streamReader = new StreamReader(response.GetResponseStream())) {
                    var result = await streamReader.ReadToEndAsync();
                    blockTicketResponse = JsonConvert.DeserializeObject<BlockTicketResponse>(result);
                }
                blockTicketResponse.BlockTicketRequestId = blockTicketRequest.Id;
                await _blockTicketResponseRepository.InsertOrUpdateAsync(blockTicketResponse);
                return blockTicketResponse;
            } catch(Exception ex) {
                throw new Exception(ex.Message);
            }
        }
        public async Task<RTCUpdatedFare> GetRTCUpdatedFare(string blockTicketKey) {
            try {
                RTCUpdatedFare rTCUpdatedFare;
                Uri url = new Uri(BusSevaBaseUrl + "getRTCUpdatedFare?blockTicketKey=" + blockTicketKey);
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.ContentType = "application/json";
                request.Method = "GET";
                DigestiveAuth(request, url);
                var response = (HttpWebResponse)await request.GetResponseAsync();
                using(var streamReader = new StreamReader(response.GetResponseStream())) {
                    var result = await streamReader.ReadToEndAsync();
                    rTCUpdatedFare = JsonConvert.DeserializeObject<RTCUpdatedFare>(result);
                }
                return rTCUpdatedFare;
            } catch(Exception ex) {
                throw new Exception(ex.Message);
            }
        }
        public async Task<SeatBooking> SeatBooking(string blockTicketKey) {
            try {
                SeatBooking seatBooking;
                Uri url = new Uri(BusSevaBaseUrl + "seatBooking?blockTicketKey=" + blockTicketKey);
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.ContentType = "application/json";
                request.Method = "GET";
                DigestiveAuth(request, url);
                var response = (HttpWebResponse)await request.GetResponseAsync();
                using(var streamReader = new StreamReader(response.GetResponseStream())) {
                    var result = await streamReader.ReadToEndAsync();
                    seatBooking = JsonConvert.DeserializeObject<SeatBooking>(result);
                }
                await _seatBookingRepository.InsertOrUpdateAsync(seatBooking);
                return seatBooking;
            } catch(Exception ex) {
                throw new Exception(ex.Message);
            }
        }
        public async Task<GetBookedTicket> GetBookedTicket(string ETSTNumber) {
            try {
                GetBookedTicket getBookedTicket;
                Uri url = new Uri(BusSevaBaseUrl + "getTicketByETSTNumber?ETSTNumber=" + ETSTNumber);
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.ContentType = "application/json";
                request.Method = "GET";
                DigestiveAuth(request, url);
                var response = (HttpWebResponse)await request.GetResponseAsync();
                using(var streamReader = new StreamReader(response.GetResponseStream())) {
                    var result = await streamReader.ReadToEndAsync();
                    getBookedTicket = JsonConvert.DeserializeObject<GetBookedTicket>(result);
                }
                await _getBookedTicketRepository.InsertOrUpdateAsync(getBookedTicket);
                return getBookedTicket;
            } catch(Exception ex) {
                throw new Exception(ex.Message);
            }
        }
        public async Task<CancelTicketResponse> CancelTicket(CancelTicketRequest cancelTicketRequest) {
            try {
                CancelTicketResponse cancelTicketResponse;
                Uri url = new Uri(BusSevaBaseUrl + "cancelTicketConfirmation");
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.ContentType = "application/json";
                request.Method = "POST";
                DigestiveAuth(request, url);
                await _cancelTicketRepository.InsertOrUpdateAsync(cancelTicketRequest);
                using(var streamWriter = new StreamWriter(request.GetRequestStream())) {
                    string json = JsonConvert.SerializeObject(cancelTicketRequest);
                    streamWriter.Write(json);
                }
                var response = (HttpWebResponse)await request.GetResponseAsync();
                using(var streamReader = new StreamReader(response.GetResponseStream())) {
                    var result = await streamReader.ReadToEndAsync();
                    cancelTicketResponse = JsonConvert.DeserializeObject<CancelTicketResponse>(result);
                }
                cancelTicketRequest.CancelTicketResponse = cancelTicketResponse;
                await _cancelTicketRepository.InsertOrUpdateAsync(cancelTicketRequest);
                return cancelTicketResponse;
            } catch(Exception ex) {
                throw new Exception(ex.Message);
            }
        }
        public async Task<CancelConfirmationTicketResponse> CancelTicketConfirmation(CancelConfirmationTicketRequest cancelTicketRequest) {
            try {
                CancelConfirmationTicketResponse cancelTicketResponse;
                Uri url = new Uri(BusSevaBaseUrl + "cancelTicket");
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.ContentType = "application/json";
                request.Method = "POST";
                DigestiveAuth(request, url);
                using(var streamWriter = new StreamWriter(request.GetRequestStream())) {
                    string json = JsonConvert.SerializeObject(cancelTicketRequest);
                    streamWriter.Write(json);
                }
                await _cancelConfirmationTicketRepository.InsertOrUpdateAsync(cancelTicketRequest);
                var response = (HttpWebResponse)await request.GetResponseAsync();
                using(var streamReader = new StreamReader(response.GetResponseStream())) {
                    var result = await streamReader.ReadToEndAsync();
                    cancelTicketResponse = JsonConvert.DeserializeObject<CancelConfirmationTicketResponse>(result);
                }
                cancelTicketRequest.CancelConfirmationTicketResponse = cancelTicketResponse;
                await _cancelConfirmationTicketRepository.InsertOrUpdateAsync(cancelTicketRequest);
                return cancelTicketResponse;
            } catch(Exception ex) {
                throw new Exception(ex.Message);
            }
        }
        public async Task<MyPlanAndCurrentBalance> GetMyPlanAndCurrentBalance() {
            try {
                Uri url = new Uri(BusSevaBaseUrl + "getMyPlanAndBalance");
                MyPlanAndCurrentBalance myPlanAndCurrentBalance;
                var request = (HttpWebRequest)WebRequest.Create(url);
                request.ContentType = "application/json";
                request.Method = "GET";
                DigestiveAuth(request, url);
                var response = (HttpWebResponse)await request.GetResponseAsync();
                using(var streamReader = new StreamReader(response.GetResponseStream())) {
                    var result = await streamReader.ReadToEndAsync();
                    myPlanAndCurrentBalance = JsonConvert.DeserializeObject<MyPlanAndCurrentBalance>(result);
                }
                return myPlanAndCurrentBalance;
            } catch(Exception ex) {
                throw new Exception(ex.Message);
            }
        }

    }
}
