﻿using Abp.Application.Services;
using System.Threading.Tasks;
using ThumbpayGo.ETravelSmart;

namespace ThumbpayGo.Bus
{
   public interface IBusAppService: IApplicationService
    {
        Task<ApiStations> GetStations();
        Task<ApiAvailableBuses> GetAvailableBuses(string sourceCity, string destinationCity, string doj);
        Task<BusLayout> GetBusLayout(string sourceCity, string destinationCity, string doj, int inventoryType, int routeScheduleId);
        Task<BlockTicketResponse> BlockTicket(BlockTicketRequest blockTicketRequest);
        Task<RTCUpdatedFare> GetRTCUpdatedFare(string blockTicketKey);
        Task<SeatBooking> SeatBooking(string blockTicketKey);
        Task<GetBookedTicket> GetBookedTicket(string ETSTNumber);
        Task<CancelConfirmationTicketResponse> CancelTicketConfirmation(CancelConfirmationTicketRequest cancelTicketRequest);
        Task<CancelTicketResponse> CancelTicket(CancelTicketRequest cancelTicketRequest);
        Task<MyPlanAndCurrentBalance> GetMyPlanAndCurrentBalance();

    }
}
