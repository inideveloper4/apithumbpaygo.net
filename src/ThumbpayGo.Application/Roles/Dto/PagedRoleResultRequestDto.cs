﻿using Abp.Application.Services.Dto;

namespace ThumbpayGo.Roles.Dto
{
    public class PagedRoleResultRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
    }
}

