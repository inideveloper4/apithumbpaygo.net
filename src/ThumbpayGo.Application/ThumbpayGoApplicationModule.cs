﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using ThumbpayGo.Authorization;

namespace ThumbpayGo
{
    [DependsOn(
        typeof(ThumbpayGoCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class ThumbpayGoApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<ThumbpayGoAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(ThumbpayGoApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddProfiles(thisAssembly)
            );
        }
    }
}
