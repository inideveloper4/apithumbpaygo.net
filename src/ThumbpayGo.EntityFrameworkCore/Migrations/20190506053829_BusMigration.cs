﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ThumbpayGo.Migrations
{
    public partial class BusMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ApiStatus",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    message = table.Column<string>(nullable: true),
                    success = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApiStatus", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BoardDropPoint",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    location = table.Column<string>(nullable: true),
                    time = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BoardDropPoint", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "CancelConfirmationTicketRequests",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    etsTicketNo = table.Column<string>(nullable: true),
                    seatNbrsToCancelStr = table.Column<string>(nullable: true),
                    CreationTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CancelConfirmationTicketRequests", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CancelTicketRequests",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    etsTicketNo = table.Column<string>(nullable: true),
                    seatNbrsToCancelStr = table.Column<string>(nullable: true),
                    CreationTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CancelTicketRequests", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GetBookedTickets",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    apiStatusId = table.Column<int>(nullable: true),
                    ticketStatus = table.Column<string>(nullable: true),
                    inventoryType = table.Column<string>(nullable: true),
                    sourceCity = table.Column<string>(nullable: true),
                    destinationCity = table.Column<string>(nullable: true),
                    journeyDate = table.Column<string>(nullable: true),
                    departureTime = table.Column<string>(nullable: true),
                    routeScheduleId = table.Column<string>(nullable: true),
                    serviceProvider = table.Column<string>(nullable: true),
                    service_type = table.Column<string>(nullable: true),
                    serviceId = table.Column<string>(nullable: true),
                    serviceProviderContact = table.Column<string>(nullable: true),
                    boardingPoint = table.Column<string>(nullable: true),
                    droppingPoint = table.Column<string>(nullable: true),
                    ETSTNumber = table.Column<string>(nullable: true),
                    opPNR = table.Column<string>(nullable: true),
                    commPCT = table.Column<double>(nullable: false),
                    cancellationPolicy = table.Column<string>(nullable: true),
                    bookingDate = table.Column<DateTime>(nullable: false),
                    Date = table.Column<DateTime>(nullable: false),
                    refundAmount = table.Column<double>(nullable: false),
                    tripCode = table.Column<string>(nullable: true),
                    CreationTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GetBookedTickets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GetBookedTickets_ApiStatus_apiStatusId",
                        column: x => x.apiStatusId,
                        principalTable: "ApiStatus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "SeatBookings",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    opPNR = table.Column<string>(nullable: true),
                    etstnumber = table.Column<string>(nullable: true),
                    commPCT = table.Column<string>(nullable: true),
                    totalFare = table.Column<decimal>(nullable: false),
                    cancellationPolicy = table.Column<string>(nullable: true),
                    tripCode = table.Column<int>(nullable: false),
                    apiStatusId = table.Column<int>(nullable: true),
                    CreationTime = table.Column<DateTime>(nullable: false),
                    inventoryType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SeatBookings", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SeatBookings_ApiStatus_apiStatusId",
                        column: x => x.apiStatusId,
                        principalTable: "ApiStatus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "BlockTicketRequests",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    sourceCity = table.Column<string>(nullable: true),
                    destinationCity = table.Column<string>(nullable: true),
                    doj = table.Column<DateTime>(nullable: false),
                    routeScheduleId = table.Column<long>(nullable: false),
                    boardingPointid = table.Column<long>(nullable: true),
                    droppingPointid = table.Column<long>(nullable: true),
                    customerName = table.Column<string>(nullable: true),
                    customerLastName = table.Column<string>(nullable: true),
                    customerEmail = table.Column<string>(nullable: true),
                    customerPhone = table.Column<string>(nullable: true),
                    emergencyPhNumber = table.Column<string>(nullable: true),
                    customerAddress = table.Column<string>(nullable: true),
                    inventoryType = table.Column<int>(nullable: false),
                    CreationTime = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlockTicketRequests", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BlockTicketRequests_BoardDropPoint_boardingPointid",
                        column: x => x.boardingPointid,
                        principalTable: "BoardDropPoint",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_BlockTicketRequests_BoardDropPoint_droppingPointid",
                        column: x => x.droppingPointid,
                        principalTable: "BoardDropPoint",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CancelConfirmationTicketResponses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CancelConfirmationTicketRequestId = table.Column<int>(nullable: false),
                    totalTicketFare = table.Column<double>(nullable: false),
                    totalRefundAmount = table.Column<string>(nullable: true),
                    cancelChargesPercentage = table.Column<string>(nullable: true),
                    cancellationCharges = table.Column<double>(nullable: false),
                    apiStatusId = table.Column<int>(nullable: true),
                    cancellable = table.Column<bool>(nullable: false),
                    partiallyCancellable = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CancelConfirmationTicketResponses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CancelConfirmationTicketResponses_CancelConfirmationTicketRequests_CancelConfirmationTicketRequestId",
                        column: x => x.CancelConfirmationTicketRequestId,
                        principalTable: "CancelConfirmationTicketRequests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CancelConfirmationTicketResponses_ApiStatus_apiStatusId",
                        column: x => x.apiStatusId,
                        principalTable: "ApiStatus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "CancelTicketResponses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CancelTicketRequestId = table.Column<int>(nullable: false),
                    totalTicketFare = table.Column<double>(nullable: false),
                    totalRefundAmount = table.Column<string>(nullable: true),
                    cancelChargesPercentage = table.Column<string>(nullable: true),
                    cancellationCharges = table.Column<double>(nullable: false),
                    apiStatusId = table.Column<int>(nullable: true),
                    cancellable = table.Column<bool>(nullable: false),
                    partiallyCancellable = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CancelTicketResponses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CancelTicketResponses_CancelTicketRequests_CancelTicketRequestId",
                        column: x => x.CancelTicketRequestId,
                        principalTable: "CancelTicketRequests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CancelTicketResponses_ApiStatus_apiStatusId",
                        column: x => x.apiStatusId,
                        principalTable: "ApiStatus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TravelerDetails",
                columns: table => new
                {
                    GetBookedTicketId = table.Column<int>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    lastName = table.Column<string>(nullable: true),
                    seatNo = table.Column<string>(nullable: true),
                    age = table.Column<long>(nullable: false),
                    gender = table.Column<string>(nullable: true),
                    fare = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TravelerDetails", x => x.GetBookedTicketId);
                    table.ForeignKey(
                        name: "FK_TravelerDetails_GetBookedTickets_GetBookedTicketId",
                        column: x => x.GetBookedTicketId,
                        principalTable: "GetBookedTickets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BlockSeatPaxDetail",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BlockTicketRequestId = table.Column<int>(nullable: false),
                    age = table.Column<int>(nullable: false),
                    name = table.Column<string>(nullable: true),
                    seatNbr = table.Column<string>(nullable: true),
                    sex = table.Column<string>(nullable: true),
                    fare = table.Column<decimal>(nullable: false),
                    serviceTaxAmount = table.Column<decimal>(nullable: false),
                    operatorServiceChargeAbsolute = table.Column<decimal>(nullable: false),
                    totalFareWithTaxes = table.Column<decimal>(nullable: false),
                    ladiesSeat = table.Column<bool>(nullable: false),
                    lastName = table.Column<string>(nullable: true),
                    mobile = table.Column<string>(nullable: true),
                    title = table.Column<string>(nullable: true),
                    email = table.Column<string>(nullable: true),
                    idType = table.Column<string>(nullable: true),
                    idNumber = table.Column<string>(nullable: true),
                    nameOnId = table.Column<string>(nullable: true),
                    primary = table.Column<bool>(nullable: false),
                    ac = table.Column<bool>(nullable: false),
                    sleeper = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlockSeatPaxDetail", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BlockSeatPaxDetail_BlockTicketRequests_BlockTicketRequestId",
                        column: x => x.BlockTicketRequestId,
                        principalTable: "BlockTicketRequests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BlockTicketResponses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BlockTicketRequestId = table.Column<int>(nullable: false),
                    blockTicketKey = table.Column<string>(nullable: true),
                    inventoryType = table.Column<string>(nullable: true),
                    apiStatusId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlockTicketResponses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BlockTicketResponses_BlockTicketRequests_BlockTicketRequestId",
                        column: x => x.BlockTicketRequestId,
                        principalTable: "BlockTicketRequests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_BlockTicketResponses_ApiStatus_apiStatusId",
                        column: x => x.apiStatusId,
                        principalTable: "ApiStatus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BlockSeatPaxDetail_BlockTicketRequestId",
                table: "BlockSeatPaxDetail",
                column: "BlockTicketRequestId");

            migrationBuilder.CreateIndex(
                name: "IX_BlockTicketRequests_boardingPointid",
                table: "BlockTicketRequests",
                column: "boardingPointid");

            migrationBuilder.CreateIndex(
                name: "IX_BlockTicketRequests_droppingPointid",
                table: "BlockTicketRequests",
                column: "droppingPointid");

            migrationBuilder.CreateIndex(
                name: "IX_BlockTicketResponses_BlockTicketRequestId",
                table: "BlockTicketResponses",
                column: "BlockTicketRequestId");

            migrationBuilder.CreateIndex(
                name: "IX_BlockTicketResponses_apiStatusId",
                table: "BlockTicketResponses",
                column: "apiStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_CancelConfirmationTicketResponses_CancelConfirmationTicketRequestId",
                table: "CancelConfirmationTicketResponses",
                column: "CancelConfirmationTicketRequestId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CancelConfirmationTicketResponses_apiStatusId",
                table: "CancelConfirmationTicketResponses",
                column: "apiStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_CancelTicketResponses_CancelTicketRequestId",
                table: "CancelTicketResponses",
                column: "CancelTicketRequestId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CancelTicketResponses_apiStatusId",
                table: "CancelTicketResponses",
                column: "apiStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_GetBookedTickets_apiStatusId",
                table: "GetBookedTickets",
                column: "apiStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_SeatBookings_apiStatusId",
                table: "SeatBookings",
                column: "apiStatusId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BlockSeatPaxDetail");

            migrationBuilder.DropTable(
                name: "BlockTicketResponses");

            migrationBuilder.DropTable(
                name: "CancelConfirmationTicketResponses");

            migrationBuilder.DropTable(
                name: "CancelTicketResponses");

            migrationBuilder.DropTable(
                name: "SeatBookings");

            migrationBuilder.DropTable(
                name: "TravelerDetails");

            migrationBuilder.DropTable(
                name: "BlockTicketRequests");

            migrationBuilder.DropTable(
                name: "CancelConfirmationTicketRequests");

            migrationBuilder.DropTable(
                name: "CancelTicketRequests");

            migrationBuilder.DropTable(
                name: "GetBookedTickets");

            migrationBuilder.DropTable(
                name: "BoardDropPoint");

            migrationBuilder.DropTable(
                name: "ApiStatus");
        }
    }
}
