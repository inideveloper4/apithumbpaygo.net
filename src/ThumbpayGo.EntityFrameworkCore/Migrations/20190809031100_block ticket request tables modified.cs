﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ThumbpayGo.Migrations
{
    public partial class blockticketrequesttablesmodified : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BlockTicketRequests_BoardDropPoint_boardingPointid",
                table: "BlockTicketRequests");

            migrationBuilder.DropForeignKey(
                name: "FK_BlockTicketRequests_BoardDropPoint_droppingPointid",
                table: "BlockTicketRequests");

            migrationBuilder.DropForeignKey(
                name: "FK_BlockTicketResponses_ApiStatus_apiStatusId",
                table: "BlockTicketResponses");

            migrationBuilder.DropTable(
                name: "BoardDropPoint");

            migrationBuilder.DropIndex(
                name: "IX_BlockTicketRequests_boardingPointid",
                table: "BlockTicketRequests");

            migrationBuilder.DropIndex(
                name: "IX_BlockTicketRequests_droppingPointid",
                table: "BlockTicketRequests");

            migrationBuilder.DropColumn(
                name: "boardingPointid",
                table: "BlockTicketRequests");

            migrationBuilder.DropColumn(
                name: "droppingPointid",
                table: "BlockTicketRequests");

            migrationBuilder.CreateTable(
                name: "ApiStatusData",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    BlockTicketRequestId = table.Column<int>(nullable: false),
                    message = table.Column<string>(nullable: true),
                    success = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ApiStatusData", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ApiStatusData_BlockTicketRequests_BlockTicketRequestId",
                        column: x => x.BlockTicketRequestId,
                        principalTable: "BlockTicketRequests",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ApiStatusData_BlockTicketRequestId",
                table: "ApiStatusData",
                column: "BlockTicketRequestId");

            migrationBuilder.AddForeignKey(
                name: "FK_BlockTicketResponses_ApiStatusData_apiStatusId",
                table: "BlockTicketResponses",
                column: "apiStatusId",
                principalTable: "ApiStatusData",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BlockTicketResponses_ApiStatusData_apiStatusId",
                table: "BlockTicketResponses");

            migrationBuilder.DropTable(
                name: "ApiStatusData");

            migrationBuilder.AddColumn<long>(
                name: "boardingPointid",
                table: "BlockTicketRequests",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "droppingPointid",
                table: "BlockTicketRequests",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "BoardDropPoint",
                columns: table => new
                {
                    id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    location = table.Column<string>(nullable: true),
                    time = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BoardDropPoint", x => x.id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BlockTicketRequests_boardingPointid",
                table: "BlockTicketRequests",
                column: "boardingPointid");

            migrationBuilder.CreateIndex(
                name: "IX_BlockTicketRequests_droppingPointid",
                table: "BlockTicketRequests",
                column: "droppingPointid");

            migrationBuilder.AddForeignKey(
                name: "FK_BlockTicketRequests_BoardDropPoint_boardingPointid",
                table: "BlockTicketRequests",
                column: "boardingPointid",
                principalTable: "BoardDropPoint",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BlockTicketRequests_BoardDropPoint_droppingPointid",
                table: "BlockTicketRequests",
                column: "droppingPointid",
                principalTable: "BoardDropPoint",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BlockTicketResponses_ApiStatus_apiStatusId",
                table: "BlockTicketResponses",
                column: "apiStatusId",
                principalTable: "ApiStatus",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
