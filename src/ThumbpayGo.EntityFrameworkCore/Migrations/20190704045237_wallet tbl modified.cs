﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ThumbpayGo.Migrations
{
    public partial class wallettblmodified : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "UserId",
                table: "WhitelevelUsers",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<DateTime>(
                name: "LastModificationTime",
                table: "Wallets",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_WhitelevelUsers_UserId",
                table: "WhitelevelUsers",
                column: "UserId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_WhitelevelUsers_AbpUsers_UserId",
                table: "WhitelevelUsers",
                column: "UserId",
                principalTable: "AbpUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WhitelevelUsers_AbpUsers_UserId",
                table: "WhitelevelUsers");

            migrationBuilder.DropIndex(
                name: "IX_WhitelevelUsers_UserId",
                table: "WhitelevelUsers");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "WhitelevelUsers");

            migrationBuilder.DropColumn(
                name: "LastModificationTime",
                table: "Wallets");
        }
    }
}
