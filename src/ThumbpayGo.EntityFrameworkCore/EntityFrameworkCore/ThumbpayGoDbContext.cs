﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using ThumbpayGo.Authorization.Roles;
using ThumbpayGo.Authorization.Users;
using ThumbpayGo.MultiTenancy;
using ThumbpayGo.ETravelSmart;
using ThumbpayGo.Authorization;

namespace ThumbpayGo.EntityFrameworkCore
{
    public class ThumbpayGoDbContext : AbpZeroDbContext<Tenant, Role, User, ThumbpayGoDbContext>
    {
        /* Define a DbSet for each entity of the application */
        public DbSet<BlockTicketRequest> BlockTicketRequests { get; set; }
        public DbSet<BlockTicketResponse> BlockTicketResponses { get; set; }
        public DbSet<CancelTicketRequest> CancelTicketRequests { get; set; }
        public DbSet<CancelTicketResponse> CancelTicketResponses { get; set; }
        public DbSet<CancelConfirmationTicketRequest> CancelConfirmationTicketRequests { get; set; }
        public DbSet<CancelConfirmationTicketResponse> CancelConfirmationTicketResponses { get; set; }
        public DbSet<GetBookedTicket> GetBookedTickets { get; set;}
        public DbSet<SeatBooking> SeatBookings { get; set; }
        public DbSet<WhitelevelUser> WhitelevelUsers { get; set; }
        public DbSet<Wallet> Wallets { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public ThumbpayGoDbContext(DbContextOptions<ThumbpayGoDbContext> options)
            : base(options)
        {
        }
    }
}
