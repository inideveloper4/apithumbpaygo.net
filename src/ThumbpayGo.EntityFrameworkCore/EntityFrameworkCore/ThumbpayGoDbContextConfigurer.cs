using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace ThumbpayGo.EntityFrameworkCore
{
    public static class ThumbpayGoDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<ThumbpayGoDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<ThumbpayGoDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
