﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using ThumbpayGo.Configuration;
using ThumbpayGo.Web;

namespace ThumbpayGo.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class ThumbpayGoDbContextFactory : IDesignTimeDbContextFactory<ThumbpayGoDbContext>
    {
        public ThumbpayGoDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<ThumbpayGoDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            ThumbpayGoDbContextConfigurer.Configure(builder, configuration.GetConnectionString(ThumbpayGoConsts.ConnectionStringName));

            return new ThumbpayGoDbContext(builder.Options);
        }
    }
}
